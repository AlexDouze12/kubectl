FROM alpine:3.13

ENV KUBECTL_VERSION=1.20.2
ENV HELM_VERSION=3.4.0

RUN wget https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl && \
    chmod +x /usr/local/bin/kubectl && \
    wget https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    tar xvzf helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    cp linux-amd64/helm /usr/local/bin/helm && \
    chmod +x /usr/local/bin/helm
